import Card from "../card/Card";
import "./Line.css";
import { stockPairs } from "../pairs/pairs";

type Props = {
  numberCart: number[];
  double: (arg: string) => void;
  turnDecision: Set<string>[];
};
export default function Line({ numberCart, double, turnDecision }: Props) {
  return (
    <div className="line">
      {numberCart.map((number) => (
        <Card
          click={double}
          result={turnDecision}
          faceOn={stockPairs[number]}
        />
      ))}
    </div>
  );
}
