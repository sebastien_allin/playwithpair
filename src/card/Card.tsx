import { useEffect, useState } from "react";
import faceOff from "../assets/BAS.jpg";

type Props = {
  faceOn: string;
  result: Set<string>[];
  click: (arg: string) => void;
};
export default function Card({ faceOn, result, click }: Props) {
  const [turn, setTurn] = useState(false);

 


  useEffect(() => {
    if (turn) {
       
      click(faceOn);
     
    }
   
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [turn]);

  useEffect(() =>{
  if(result[0]?.has(faceOn)){
    setTimeout(() =>{
      setTurn(false)
    }, 800) 
  }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[result])

  const handleClick = () => {
    setTurn(!turn);
    
  };


  return (
    <div>
      <img
        width="100"
        height="150"
        onClick={handleClick}
        src={turn ? faceOn : faceOff}
        alt="card"
      />
    </div>
  );
}
