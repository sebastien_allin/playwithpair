import { useEffect, useState } from "react";
import "./App.css";
import Line from "./line/Line";
import { distribute } from "./pairs/pairs";

function App() {
  const [tirage, setTirage] = useState<number[]>([]);
  const [turnDecision, setTurnDecision] = useState<Set<string>[]>([]);
  const [result, setResult] = useState<string[]>([]);

  if (result?.length === 2) {
    const goal = [new Set(result)];
    console.log(goal);
    if (goal[0]?.size > 1) {
      setTurnDecision(goal), setResult([]);
    } else {
      setResult([]);
    }
  }
  function double(card: string) {
    setResult([...result, card]);
    
  }

  useEffect(() => {
    setTirage(distribute(16));
  }, []);
  return (
    <>
      <Line
        double={double}
        turnDecision={turnDecision}
        numberCart={tirage.slice(12, 16)}
      />
      <Line
        double={double}
        turnDecision={turnDecision}
        numberCart={tirage.slice(8, 12)}
      />
      <Line
        double={double}
        turnDecision={turnDecision}
        numberCart={tirage.slice(4, 8)}
      />
      <Line
        double={double}
        turnDecision={turnDecision}
        numberCart={tirage.slice(0, 4)}
      />
    </>
  );
}

export default App;
