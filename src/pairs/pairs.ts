import pair1 from "../assets/pair1.jpg";
import pair2 from "../assets/pair2.jpg";
import pair3 from "../assets/pair3.jpg";
import pair4 from "../assets/pair4.jpg";
import pair5 from "../assets/pair5.jpg";
import pair6 from "../assets/pair6.jpg";
import pair7 from "../assets/pair7.jpg";
import pair8 from "../assets/pair8.jpg";

export const stockPairs = [
  pair1,
  pair2,
  pair3,
  pair4,
  pair5,
  pair6,
  pair7,
  pair8,
];

export function distribute(numbers: number) {
  const all = [];

  for (let i = 0; i < numbers; i++) {
    const tirage = Math.floor(Math.random() * 8);
    const verif = all.filter((el) => el === tirage);
    if (verif.length < 2) {
      all.push(tirage);
    } else {
      i--;
    }
  }
  return all;
}
